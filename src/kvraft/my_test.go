package kvraft

import (
	// "strings"
	"fmt"
	"testing"
)

func TestNrand(t *testing.T) {
	for i := 0; i < 10; i++ {
		fmt.Println(nrand())
	}
}

// func TestOp(t *testing.T) {
// 	op := Op{Key: "boo", Op: "GET"}
// 	fmt.Println(len(op.Value))
// 	fmt.Println(len(strings.Join([]string{}, " ")))
// }

// func TestEqual(t *testing.T) {
// 	op := Op{"boo", "foo", "GET"}
// 	op2 := Op{"boo", "foo", "GET"}
// 	fmt.Println(op == op2) // true
// }

func TestDB(t *testing.T) {
	db := make(map[string][]string)
	db["a"] = append(db["a"], "1")
	db["a"] = append(db["a"], "2")
	fmt.Println(db["a"][1])
	db["a"] = []string{}
	fmt.Println(db)
}

func TestMap(t *testing.T) {
	storage := make(map[int]int)
	storage[2] = 3
	if val, ok := storage[4]; ok {
		fmt.Printf("val=%d", val)
	}
	val, ok := storage[4]
	fmt.Printf("%d, %t", val, ok)
}

// func TestResponsememo(t *testing.T) {
// 	res1 := CommandResponse{CommandID: 2, Value: "Hello"}
// 	res2 := CommandResponse{CommandID: 3, Value: "World"}
// 	memo := make(ResponseMemo)
// 	vec := make(ResponseVector, 0)
// 	fmt.Println(len(vec))
// 	memo[1] = make(ResponseVector, 0)
// 	memo[1] = append(memo[1], &res1)
// 	memo[1] = append(memo[1], &res2)
// 	clearResponse(&memo, 1, 3)
// 	fmt.Printf("len(memo[1]) = %d", len(memo[1]))
// }

func TestDeletemap(t *testing.T) {
	memo := make(map[int]int)
	memo[2] = 3
	memo[4] = 5
	memo2 := memo
	memo2[5] = 6
	if val, ok := memo[5]; ok {
		fmt.Println(val)
	}
	memo[1] += 2
	if val, ok := memo[1]; ok {
		fmt.Println(val)
	}
	fmt.Println(memo[3])
}

type Student struct {
	age int
	name string
}

func pp(name *map[int]string) {
	fmt.Printf("%v", name)
	fmt.Println((*name)[1])
}

func TestType(t *testing.T) {
	// student := new(Student)
	// student.age = 1
	// student.name = "Victor"
	// pp(&student.name)
	// kv := make(map[int]string)
	// kv[1] = "hello"
	// pp(&kv)
	arr := []int{1, 2, 3, 4}
	fmt.Println(arr[4:])
	for i := 0; i < 3; i++ {
		fmt.Println(i)
	}
}