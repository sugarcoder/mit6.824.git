package mr

//
// RPC definitions.
//
// remember to capitalize all names.
//

import "os"
import "strconv"
// import "bytes"
// import "encoding/gob"
// import "log"

//
// example to show how to declare the arguments
// and reply for an RPC.
//

type ExampleArgs struct {
	X int
}

type ExampleReply struct {
	Y int
}

// Add your RPC definitions here.
const (
	MAP = 1
	REDUCE = 2
)

type RegistArgs struct {

}

type RegistResp struct {
	WorkerID int
}

// RequestArgs worker请求的类型
type RequestArgs struct {
	TaskNum int // 返回被分配的任务索引，初始请求时为空
	JobType int // 任务类型 1-map 2-reduce
	WorkerID int // 这个worker的ID
}

// master的应答
type ResponseType struct {
	NReduce int // master的用户参数，取模用
	JobType int
	BucketName string // 分配的任务名称 （map任务需要读取的文件名)
	TaskNum int 	// 任务号 
					// 1、对于map任务，master数据结构中的files的下标 + 1
					// 2、对于reduce任务，taskNum在 [0, NReduce) 区间内
}

func counter() (f func() int) {
	i := 0
	return func() int {
		i += 1
		return i
	}
}

// generate a unique id for a worker or a Job
var uniqueID = counter()

// Deprecated: old style of decoding.
// 编码好的json格式数据写入内存的buffer中
// func encode(kvs []KeyValue) []byte {
// 	buf := bytes.Buffer{}
// 	encoder := gob.NewEncoder(&buf)
// 	err := encoder.Encode(&kvs)
// 	if err != nil {
// 		log.Fatalln("cannot encode intermediate result", err)
// 	}
// 	return buf.Bytes()
// }

// func decode(b []byte) []KeyValue {
// 	buf := bytes.Buffer{}
// 	buf.Write(b)
// 	decoder := gob.NewDecoder(&buf)
// 	kvs := []KeyValue{}
// 	err := decoder.Decode(&kvs)
// 	if err != nil {
// 		log.Fatalln("cannot decode intermediate result", err)
// 	}
// 	return kvs
// }


// Cook up a unique-ish UNIX-domain socket name
// in /var/tmp, for the master.
// Can't use the current directory since
// Athena AFS doesn't support UNIX-domain sockets.
func masterSock() string {
	s := "/var/tmp/824-mr-"
	s += strconv.Itoa(os.Getuid())
	return s
}
