package raft

import (
	"log"
	"time"
	"fmt"
)

// Debugging
const Debug = 0

func RPrintf(rf *Raft, format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		format = fmt.Sprintf("[%d] %v %d: ", rf.currentTerm, rf.role, rf.me) + format
		log.Printf(format, a...)
	}
	return
}

func DPrintf(format string, a ...interface{}) (n int, err error) {
	if Debug > 0 {
		log.Printf(format, a...)
	}
	return
}

const GUARDTIMEOUT = 20 * time.Millisecond

func min(a, b int) int {
	if a > b {
		return b
	}
	return a
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
